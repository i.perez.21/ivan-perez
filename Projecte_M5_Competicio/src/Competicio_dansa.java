import java.util.Scanner;

/**
 * <h2>Competici� Dansa DomJudge</h2>
 * 
 * @version 1-2020
 * @author iperez
 * @since 25-2-2020
 */
public class Competicio_dansa {
	
	/**
	 * M�tode que engloba tot el programa, cont� les tres variables principals que serveixen per demanar les dades, una altra variable que �s la que ens indicar� la posici� del guanyador, i una altra que serveix per anar substituint la puntuaci� si aquesta �s major que l'anterior..
	 * @param args Cont� les variables principals i els bucles per fer la comprovaci� del guanyador.
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int casos = reader.nextInt(); //Variable que indica els casos que vol realitzar l'usuari.
		int cont = 0; //Variable que serveix per indicar la posici� del guanyador.
		int aux = 0; //Variable que serveix per anar canviant la puntuaci� si �s major que l'anterior.
		for (int i = 0; i < casos; i++) {
			int ballarins = reader.nextInt(); //Variable que indica el nombre de ballarins que participen a la competici�.
			for (int x = 0; x < ballarins; x++) {
				int puntuacio = reader.nextInt(); //Variable que indica la puntuaci� que han obtingut els ballarins durant la competici�.
				if (puntuacio > aux) {
					aux = puntuacio;
					cont = x + 1;
				}
			}
			System.out.println(cont);
			aux = 0;
		}
		reader.close();
	}
}

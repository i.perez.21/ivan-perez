import java.util.Scanner;
import java.util.Random;
public class BuscaMines {
	static Scanner reader = new Scanner(System.in);
	static Random rand = new Random();
	static String jugador;
	static String[] ranking;
	static int[] mides = {0,0,0};
	public static void main(String[] args) {
		char[][] tauler = new char[0][0];
		char[][] secret = new char[0][0];
		int opcio = 1;
		boolean op = false;
		while (opcio != 0) {
			try {
				opcio = mostraMenu();
				switch(opcio) {
				case 1:
					mostrarAjuda();
					break;
				case 2:
					mides = opcions();
					tauler = new char[mides[0]][mides[1]];
					secret = new char[mides[0]][mides[1]];
					op = true;
					break;
				case 3:
					if(op) {
						jugar(secret, tauler);
					}else {
						System.out.println("Primer has d'indicar els parmetres de la partida!\n");
					}
					break;
				case 4:
					
					break;
				case 0:
					System.out.println("Sortint...");
					break;
				default:
					System.out.println("No has triat una opci correcta.\n");
				}
			}catch(Exception e) {
				System.out.println("[ERROR] Introdueix una dada vlida.");
				reader.nextLine();
			}
		}
	}
	static int mostraMenu() {
		int escull;
		System.out.println("Escull una d'aquestes opcions:\n");
		System.out.println("1. Mostrar ajuda");
		System.out.println("2. Opcions");
		System.out.println("3. Jugar Partida");
		System.out.println("4. Veure Rankings");
		System.out.println("0. Sortir");
		escull = reader.nextInt();
		return escull;
	}
	static void mostrarAjuda() {
		System.out.println("El joc consisteix a netejar totes les caselles d'una pantalla que no amaguin una mina.\r\n");
		System.out.print("Algunes caselles tenen un nmero. Aquest nmero indica les mines que sumen totes les caselles circumdants.\r\n" );
		System.out.println("Si es descobreix una casella sense nmero ens indica que cap de les caselles venes t mina i aquestes es descobreixen automticament.\r\n");
		System.out.println("Si es descobreix una casella amb mina es perd la partida.\n");
	}
	static int[] opcions() {
		System.out.println("Introdueix el teu nom:");
		jugador = reader.next();
		while(mides[0] < 3 || mides[0] > 25) {
			System.out.println("Indica les files que vols (entre 3 i 25): ");
			mides[0] = reader.nextInt();
		}
		while(mides[1] < 3 || mides[1] > 25) {
			System.out.println("Indica les columnes que vols (entre 3 i 25): ");
			mides[1] = reader.nextInt();
		}
		while(mides[2] < 1 || mides[2] > mides[0]*mides[1]/3) {
			System.out.println("Indica les mines que vols (entre 1 i " + mides[0]*mides[1]/3 + "):");
			mides[2] = reader.nextInt();
		}
		return mides;
	}
	static void inicialitzarMines(char[][] secret) {
		int a = 0;
		int b = 0;
		int z = 0;
		for(int i = 0; i < mides[0]; i++) {
			for(int x = 0; x < mides[1]; x++) {
				secret[i][x] = '9';
				
			}
		}
		while (z < mides[2]) {
			a = rand.nextInt(mides[0]);
			b = rand.nextInt(mides[1]);
			if(secret[a][b] == '9') {
				secret[a][b] = '*';
				z++;
			}
		}
	}
	static void inicialitzarTauler(char[][] tauler) {
		for(int i = 0; i < mides[0]; i++) {
			for(int x = 0; x < mides[1]; x++) {
				tauler[i][x] = '9';		
			}
		}
	}
	static void visualitzarCamp(char[][] tauler) {
		for(int j = 0; j < mides[0]; j++) {
			for(int y = 0; y < mides[1]; y++) {
				System.out.print(tauler[j][y]);
			}
			System.out.println();
		}
		System.out.println();
	}
	static void jugar(char[][] secret, char[][] tauler) {
		boolean partidaEnCurs = false;
		int d;
		inicialitzarMines(secret);
		inicialitzarTauler(tauler);
		visualitzarCamp(tauler);
		partidaEnCurs = true;
		
		while (partidaEnCurs == true) {
			int x = demanarCoordX();
			int y = demanarCoordY();
			tauler = descobrir(x,y,secret,tauler);
			d = tauler[x][y];
			partidaEnCurs = partidaAcabada(d, tauler);
			visualitzarCamp(tauler);
		}
		fiPartida();
	}
	static int demanarCoordX() {
		System.out.println("Indica la posici X:");
		int x = reader.nextInt() - 1;
		return x;
	}
	static int demanarCoordY() {
		System.out.println("Indica la posici Y:");
		int y = reader.nextInt() - 1;
		return y;
	}
	
	/*Repair this method*/
	
	/*static char destapar(int x, int y, char[][] secret){
		int[] columnes = new int[] { 1, -1, 0, 0, 1, 1, -1, -1 };
		int[] files = new int[] { 0, 0, 1, -1, 1, -1, -1, 1 };
		for (int i = 0; i < secret.length; i++) {
			for (int j = 0; j < secret[0].length; j++) {
				char cont = 0;
				for (int z = 0; z < 8; z++) {
					int sumfiles = i + files[z];
					int sumcolumnes = j + columnes[z];
					if (sumfiles < secret.length && sumfiles >= 0 && sumcolumnes < secret[0].length && sumcolumnes >= 0) {
						if (secret[sumfiles][sumcolumnes] == '*') {
							cont++;
						}
					}
				}
			}
		}
	}
	*/
	static char[][] descobrir(int x, int y, char[][] secret, char[][] tauler) {
		//char n = destapar(x,y,secret);
		//tauler[x][y] = n;
		return tauler;
	}
	static boolean partidaAcabada(int d, char[][] tauler) {
		int cont = 0;
		if (d == '*') {
			System.out.println("Has trobat una mina. Has perdut!");
			return false;
		}
		for(int i = 0; i < tauler.length; i++) {
			for (int x = 0; x < tauler[0].length;x++) {
				if (tauler[i][x] == '9') {
					cont = cont++;
				}
			}
		}
		if (cont == mides[2]) {
			System.out.println("Has guanyat la partida!");
		}
		return true;
	}
	static void fiPartida() {
		
	}
}